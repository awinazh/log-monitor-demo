# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.5/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.5/gradle-plugin/reference/html/#build-image)
* [Spring Data R2DBC](https://docs.spring.io/spring-boot/docs/2.4.5/reference/html/spring-boot-features.html#boot-features-r2dbc)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.4.5/reference/htmlsingle/#production-ready)

### Guides

The following guides illustrate how to use some features concretely:

* [Acessing data with R2DBC](https://spring.io/guides/gs/accessing-data-r2dbc/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)
* [R2DBC Homepage](https://r2dbc.io)

## How to run locally

### Pre-requisite

* Linux - ubuntu 20
* Docker Environment
* IntelliJ IDEA CE
* OpenJDK 8
* GIT

Check out project from GIT and follow instructions below.

Upload log files in following directory, those are being parsed.
```
<project-directory>/data
```

### Run docker-compose
To run docker compose locally, run following commands
```
cd <project-directory>

docker-compose up --build
```
To stop running containers, run following command
```
cd <project-directory>

docker-compose down
```
### To run from IDE
* Import project
* Update environment vars from `.env` into the `run configuration`
* Run project as spring boot application

### Access endpoints
Once application is running either via docker-compose or IDE.

To process/parse log file, hit the rest endpoint
```
http://localhost:8080/logmonitor/process/{filename}

Example: 
URL: http://localhost:8080/logmonitor/process/logfile.txt
Status: 200 OK
Response: Processing file 'logfile.txt', check logs for success/failure!
```

To retrieve alerts, hit the rest endpoint
```
http://localhost:8080/logmonitor/alerts/{flag}

Example: 
URL: http://localhost:8080/logmonitor/alerts/true
Status: 200 OK
Response:
[
    {
        "id": "scsmbstgrc",
        "duration": 8,
        "alert": true
    },
    {
        "id": "scsmbstgra",
        "type": "APPLICATION_LOG",
        "host": "12345",
        "duration": 5,
        "alert": true
    }
]
```