FROM openjdk:alpine as builder
WORKDIR /var/app
COPY ["gradlew", "/var/app/"]
COPY ["gradle", "/var/app/gradle"]
COPY ["build.gradle", "settings.gradle","/var/app/"]
COPY ["/src","/var/app/src"]
RUN ./gradlew assemble --no-daemon

FROM openjdk:alpine
WORKDIR /var/app
COPY --from=builder /var/app/build/libs/*.jar /var/app/app.jar
EXPOSE 8080
ENTRYPOINT [ \
    "sh", \
    "-c", \
    "java \
    -XX:+IgnoreUnrecognizedVMOptions \
    -XX:+PerfDisableSharedMem \
    -XX:+HeapDumpOnOutOfMemoryError \
    -XX:+ExitOnOutOfMemoryError \
    -XX:+UseG1GC \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+UseCGroupMemoryLimitForHeap \
    -XX:MaxRAMFraction=2 \
    -XX:+UseStringDeduplication \
    -jar /var/app/app.jar"]