package in.dz.logmonitor.repository;

import in.dz.logmonitor.entity.Event;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface EventRepository extends ReactiveCrudRepository<Event, String> {
  Flux<Event> findByAlert(Boolean flag);
}
