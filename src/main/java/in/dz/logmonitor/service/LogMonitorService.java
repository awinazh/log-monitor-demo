package in.dz.logmonitor.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.dz.logmonitor.entity.Event;
import in.dz.logmonitor.enums.State;
import in.dz.logmonitor.exception.LogMonitorException;
import in.dz.logmonitor.model.LogRecord;
import in.dz.logmonitor.repository.EventRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

@Slf4j
@Component
public class LogMonitorService {

  private ConcurrentHashMap<String, LogRecord> store;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private EventRepository eventRepository;

  @Value("${flux-workers}")
  private int workers;

  @Value("${timeout-in-ms}")
  private long timeout;

  public Flux<Event> alerts(Boolean flag) {
    log.info(String.format("Fetching events where alert is %s", flag));
    return this.eventRepository.findByAlert(flag);
  }

  public Mono<Void> process(@NonNull String filename) {
    store = new ConcurrentHashMap<>();
    Path inputFile = Paths.get(filename);
    return Flux.using(() -> Files.lines(inputFile), Flux::fromStream, Stream::close)
        .doOnSubscribe(subscription -> log.info(String.format("Processing file %s", filename)))
        .parallel(workers)
        .runOn(Schedulers.parallel())
        .map(s -> toLogRecord(s))
        .filter(logRecord -> isLogRecordExistsInLocalStore(logRecord))
        .map(logRecord -> toEvent(logRecord))
        .flatMap(eventRepository::save)
        .doOnNext(event -> cleanup(event))
        .then();
  }

  /**
   * Remove LogRecord form local store
   * Call garbage collector to raise priority
   *
   * @param event
   */
  private void cleanup(Event event) {
    store.remove(event.getId());
    System.gc();
  }

  /**
   * isLogRecordExistsInLocalStore check if other log record(start or finish) exists in local store.
   *
   * @param record
   * @return
   */
  protected boolean isLogRecordExistsInLocalStore(@NonNull LogRecord record) {
    if (store.get(record.getId()) != null) {
      log.debug(String.format("Found records in store: %s", record.getId()));
      return true;
    } else {
      store.put(record.getId(), record);
      log.debug(String.format("Records in store: %s", store.size()));
      return false;
    }
  }

  /**
   * toLogRecord Converts JSON string to LogRecord object.
   *
   * @param record
   * @return
   */
  protected LogRecord toLogRecord(@NonNull String record) {
    try {
      log.debug(String.format("Processing record: %s", record));
      LogRecord logRecord = this.objectMapper.readValue(record, LogRecord.class);
      log.debug(String.format("Mapped log record: %s", logRecord.toString()));
      return logRecord;
    } catch (JsonProcessingException e) {
      log.debug("Error while parsing log record!", e);
      throw new LogMonitorException(e);
    }
  }

  /**
   * toEvent Method maps log record to event assuming both the events(start & finish) are received.
   *
   * @param record
   * @return
   */
  protected Event toEvent(@NonNull LogRecord record) {
    log.debug(String.format("Processing log record: %s", record));
    long duration = calculateEventDuration(record);
    Event event = Event
        .builder()
        .id(record.getId())
        .duration(duration)
        .type(record.getType())
        .host(record.getHost())
        .alert(duration > timeout)
        .insert(true)
        .build();
    log.info(event.toString());
    return event;
  }

  /**
   * calculateEventDuration Calculates duration assuming both events are received.
   *
   * @param record
   * @return
   */
  protected long calculateEventDuration(@NonNull LogRecord record) {
    LogRecord storeRecord = this.store.get(record.getId());
    if (storeRecord == null)
      throw new LogMonitorException(String.format("Event missing from local store for %s", record.getId()));
    else if (record.getState().equals(State.STARTED))
      return storeRecord.getTimestamp().getTime() - record.getTimestamp().getTime();
    else
      return record.getTimestamp().getTime() - storeRecord.getTimestamp().getTime();
  }
}