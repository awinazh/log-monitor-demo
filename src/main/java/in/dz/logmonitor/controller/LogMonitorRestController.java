package in.dz.logmonitor.controller;

import in.dz.logmonitor.entity.Event;
import in.dz.logmonitor.service.LogMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController()
@RequestMapping("/logmonitor")
public class LogMonitorRestController {

  @Autowired
  private LogMonitorService logMonitorService;

  @Value("${log-dir-path}")
  private String logDirPath;

  @GetMapping("/process/{filename}")
  public ResponseEntity process(@PathVariable("filename") String filename) {
    this
        .logMonitorService
        .process(String.join("/", logDirPath, filename))
        .subscribe();
    return ResponseEntity
        .ok()
        .body(String.format("Processing file '%s', check logs for success/failure!", filename));
  }

  @GetMapping("/alerts/{flag}")
  public ResponseEntity<Flux<Event>> process(@PathVariable("flag") Boolean flag) {
    return ResponseEntity.ok().body(this.logMonitorService.alerts(flag));
  }
}
