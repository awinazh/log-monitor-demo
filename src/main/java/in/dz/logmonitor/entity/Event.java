package in.dz.logmonitor.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event implements Persistable<String> {
  @Id
  private String id;
  private String type;
  private String host;
  private Long duration;
  private Boolean alert;

  @ToString.Exclude
  @Transient
  @JsonIgnore
  private boolean insert;

  @Override
  @JsonIgnore
  public boolean isNew() {
    return insert || id == null;
  }
}
