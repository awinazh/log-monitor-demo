package in.dz.logmonitor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import in.dz.logmonitor.enums.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogRecord {
  private String id;
  private State state;
  private String type;
  private String host;
  private Date timestamp;
}
