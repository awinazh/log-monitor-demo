package in.dz.logmonitor.exception;

public class LogMonitorException extends RuntimeException {
  public LogMonitorException(Throwable t) {
    super(t);
  }

  public LogMonitorException(String msg) {
    super(msg);
  }
}
