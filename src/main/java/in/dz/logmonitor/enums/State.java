package in.dz.logmonitor.enums;

public enum State {
  STARTED,
  FINISHED
}