package in.dz.logmonitor.repository;

import in.dz.logmonitor.entity.Event;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.H2Dialect;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;
import reactor.test.StepVerifier;

import java.util.UUID;

@ExtendWith(SpringExtension.class)
@DataR2dbcTest
@ActiveProfiles("test")
public class EventRepositoryTest {
  @Autowired
  private DatabaseClient databaseClient;

  @Autowired
  private EventRepository eventRepository;

  @Test
  public void testGivenValidEventRecordWhenSavedThenEventShouldPersists() {
    String id = UUID.randomUUID().toString();
    Event event = Event
        .builder()
        .id(id)
        .alert(false)
        .host("host")
        .type("type")
        .duration(Long.parseLong("7"))
        .insert(true)
        .build();
    R2dbcEntityTemplate template = new R2dbcEntityTemplate(databaseClient, H2Dialect.INSTANCE);
    eventRepository.save(event)
        .as(StepVerifier::create)
        .assertNext(actual -> {
          Assert.isTrue(actual.getId().equalsIgnoreCase(id), "id mismatch");
        })
        .verifyComplete();
    eventRepository.findById(id)
        .as(StepVerifier::create)
        .assertNext(actual -> {
          Assert.isTrue(actual.getId().equalsIgnoreCase(id), "id mismatch");
        }).verifyComplete();
  }
}
