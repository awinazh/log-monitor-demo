package in.dz.logmonitor.service;

import in.dz.logmonitor.repository.EventRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.Assert;
import reactor.test.StepVerifier;

@SpringBootTest
@ActiveProfiles("test")
public class LogMonitorServiceTest {

  @Value("${log-dir-path}")
  private String logDirPath;

  @Autowired
  private LogMonitorService logMonitorService;

  @Autowired
  private EventRepository eventRepository;

  @Test
  public void testProcess() {
    StepVerifier
        .create(logMonitorService
            .process(String.join("/", logDirPath, "logfile.txt")))
        .verifyComplete();

    eventRepository
        .findAll()
        .as(StepVerifier::create)
        .assertNext(event -> Assert.notNull(event, "No event found!"))
        .expectNext();
  }
}
